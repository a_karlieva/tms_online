FROM jenkins/jenkins:lts
USER root
RUN mkdir /my_app
WORKDIR /my_app
# Updating apt to see and install Google Chrome
RUN apt-get -y update
RUN apt-get install -y apt wget
# Adding trusting keys to apt for repositories
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
# Adding Google Chrome to the repositories
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
# Magic happens
RUN apt-get -y update
RUN apt-get install -y google-chrome-stable
# Installing Unzip
RUN apt-get install -yqq unzip
# Download the Chrome Driver
RUN wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
# Unzip the Chrome Driver into /usr/local/bin directory
RUN unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/
# install allure
RUN curl -o allure-2.16.1.tgz -Ls https://github.com/allure-framework/allure2/releases/download/2.16.1/allure-2.16.1.tgz
RUN tar -zxvf allure-2.16.1.tgz -C /opt/
RUN ln -s /opt/allure-2.16.1/bin/allure /usr/bin/allure
RUN allure --version
# set display port to avoid crash
ENV DISPLAY=:99
COPY requirements.txt /my_app
RUN apt-get update
RUN apt-get install -y --force-yes python3-pip
RUN python3 -m pip install -r requirements.txt