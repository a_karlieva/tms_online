import time
from .base_page import BasePage
from .page_locators.add_users_locators import UsersPageLocators
from .change_user_page import ChangeUsersPage


class AddUsersPage(BasePage):
    """
    Class of AddUserPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def add_user(self, login, password):
        """
        Function of adding new user
        :param login: user new login
        :param password: user new password
        :return: open of ChangeUsersPage
        """
        login_field = self.find_element(UsersPageLocators.LOCATOR_USERNAME)
        password_field = self.find_element(UsersPageLocators.LOCATOR_PASSWORD)
        password_conf_field = self.find_element(
            UsersPageLocators.LOCATOR_CONFIRM_PASSWORD
        )
        login_field.send_keys(login)
        password_field.send_keys(password)
        password_conf_field.send_keys(password)
        btn = self.find_element(UsersPageLocators.LOCATOR_BTN)
        btn.click()
        time.sleep(5)
        return ChangeUsersPage(self.driver, self.driver.current_url)
