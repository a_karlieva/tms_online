from .base_page import BasePage
from .page_locators.change_user_locators import ChangeUserPageLocators


class ChangeUsersPage(BasePage):
    """
    Class of ChangeUsersPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def click_checkbox(self):
        """
        Function of checking checkboxes on the page to give permissions to user
        :return: success message
        """
        link_staff = self.find_element(
            ChangeUserPageLocators.LOCATOR_STAFF_CHECKBOX)
        link_staff.click()
        link_superuser = self.find_element(
            ChangeUserPageLocators.LOCATOR_SUPERUSER_CHECKBOX
        )
        link_superuser.click()
        save_btn = self.find_element(ChangeUserPageLocators.LOCATOR_SAVE_BTN)
        save_btn.click()
        success_message = self.find_element(
            ChangeUserPageLocators.LOCATOR_SUCCESS)
        return success_message
