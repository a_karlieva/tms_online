from .base_page import BasePage
from .page_locators.main_page_locators import MainPageLocators
from .login_page import LoginPage


class MainPage(BasePage):
    """
    Class of MainPage, inherited from the BasePage.
    """

    URL = "http://host.docker.internal:8000"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_login_page(self):
        """
        Function of opening login page
        :return: opens LoginPage
        """
        login_link = self.find_element(MainPageLocators.LOCATOR_MAIN_PAGE)
        login_link.click()
        return LoginPage(self.driver, self.driver.current_url)

    def get_img_value_list(self):
        """
        Function of getting list of images
        :return: list of images srcs
        """
        img_id = self.find_elements(MainPageLocators.LOCATOR_TOP_IMG)
        return img_id
