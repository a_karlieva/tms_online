from .base_page import BasePage


class LogoutPage(BasePage):
    """
    Class of LogoutPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title
