from .add_user_page import AddUsersPage
from .page_locators.admin_page_locators import AdminPageLocators
from .base_page import BasePage
from .logout_page import LogoutPage
from .page_locators.logout_page_locators import LogoutPageLocators
from .posts_page import PostsPage


class AdminPage(BasePage):
    """
    Class of AdminPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def get_header_name(self):
        """
        Function of getting page header
        :return: element
        """
        return self.find_element(AdminPageLocators.LOCATOR_ADMIN_HEADER)

    def go_to_add_users_page(self):
        """
        Function of getting to AddUsersPage
        :return: AddUsersPage
        """
        link = self.find_element(AdminPageLocators.LOCATOR_ADD_USER_LINK)
        link.click()
        return AddUsersPage(self.driver, self.driver.current_url)

    def logout(self):
        """
        Function of getting to LogoutPage
        :return: LogoutPage
        """
        link = self.find_element(LogoutPageLocators.LOCATOR_LOGOUT)
        link.click()
        return LogoutPage(self.driver, self.driver.current_url)

    def go_to_posts_page(self):
        """
        Function of getting to PostsPage
        :return: PostsPage
        """
        link = self.find_element(AdminPageLocators.LOCATOR_POSTS)
        link.click()
        return PostsPage(self.driver, self.driver.current_url)
