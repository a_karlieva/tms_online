from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    """
    Base page class
    """

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        """
        Function of page opening
        """
        self.driver.get(self.url)

    def find_element(self, locator, timeout=10):
        """
        Function of finding element by locator
        :param locator: locator
        :param timeout: timeout
        :return: element
        """
        try:
            ec_pr = EC.presence_of_element_located(locator)
            element = WebDriverWait(self.driver, timeout).until(ec_pr)
            return element
        except TimeoutException:
            return None

    def find_elements(self, locator, timeout=10):
        """
        Function of finding elements by locator
        :param locator: locator
        :param timeout: timeout
        :return: element
        """
        try:
            ec_pr = EC.presence_of_all_elements_located(locator)
            element = WebDriverWait(self.driver, timeout).until(ec_pr)
            return element
        except TimeoutException:
            return None
