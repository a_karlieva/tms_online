from .base_page import BasePage
from .page_locators.login_page_locators import LoginPageLocators
from .admin_page import AdminPage


class LoginPage(BasePage):
    """
    Class of LoginPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def login(self, login, password):
        """
        Function of logging user
        :param login: user's login
        :param password: user's password
        :return: open of AdminPage
        """
        login_field = self.find_element(LoginPageLocators.LOCATOR_LOGIN)
        password_field = self.find_element(LoginPageLocators.LOCATOR_PASSWORD)
        login_field.send_keys(login)
        password_field.send_keys(password)
        btn = self.find_element(LoginPageLocators.LOCATOR_BTN)
        btn.click()
        return AdminPage(self.driver, self.driver.current_url)
