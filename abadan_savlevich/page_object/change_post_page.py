from .base_page import BasePage
from .page_locators.change_post_locators import ChangePostPageLocators


class ChangePostPage(BasePage):
    """
    Class of ChangePostPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def get_img_src(self):
        """
        Function of getting img on the page
        :return: element
        """
        img = self.find_element(
            ChangePostPageLocators.LOCATOR_IMG_ID)
        return img

    def delete_post(self):
        """
        Function of deleting post
        :return: success message
        """
        btn = self.find_element(
            ChangePostPageLocators.LOCATOR_DELETE_BTN)
        btn.click()
        yes_btn = self.find_element(
            ChangePostPageLocators.LOCATOR_YES_BTN)
        yes_btn.click()
        success_message = self.find_element(
            ChangePostPageLocators.LOCATOR_SUCCESS)
        return success_message
