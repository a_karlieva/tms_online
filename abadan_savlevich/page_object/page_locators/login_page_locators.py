from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Class of LoginPage Locators
    """

    LOCATOR_LOGIN = (By.CSS_SELECTOR, "#id_username")
    LOCATOR_PASSWORD = (By.CSS_SELECTOR, "#id_password")
    LOCATOR_BTN = (By.CSS_SELECTOR, "input[type='submit']")
    LOCATOR_LOGIN_HEADER = (By.CSS_SELECTOR, "a[href='/admin/']")
