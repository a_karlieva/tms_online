from selenium.webdriver.common.by import By


class LogoutPageLocators:
    """
    Class of LogoutPage Locators
    """

    LOCATOR_LOGOUT = (By.CSS_SELECTOR, "a[href='/admin/logout/']")
