from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Class of MainPage Locators
    """

    LOCATOR_MAIN_PAGE = (By.CSS_SELECTOR, ".btn.btn-primary.my-2")
    LOCATOR_TOP_IMG = (By.CSS_SELECTOR, ".card-img-top")
