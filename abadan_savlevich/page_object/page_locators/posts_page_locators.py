from selenium.webdriver.common.by import By


class PostsPageLocators:
    """
    Class of PostsPage Locators
    """

    LOCATOR_CHECKBOX = (By.CSS_SELECTOR, ".action-checkbox :first-child")
    LOCATOR_FIRST_ROW = (By.CSS_SELECTOR, ".field-__str__ :first-child")
    LOCATOR_CHECKED_CHECKBOX = (By.CSS_SELECTOR, ".selected :first-child")
    LOCATOR_DROPDOWN = (By.CSS_SELECTOR, "select[name = 'action']")
    LOCATOR_DELETE = (By.CSS_SELECTOR, "option[value = 'delete_selected']")
    LOCATOR_GO_BTN = (By.CSS_SELECTOR, ".button")
    LOCATOR_YES = (By.CSS_SELECTOR, "input[type = 'submit']")
    LOCATOR_SUCCESS = (By.CSS_SELECTOR, ".success")
