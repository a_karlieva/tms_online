from selenium.webdriver.common.by import By


class ChangeUserPageLocators:
    """
    Class of ChangeUserPage Locators
    """

    LOCATOR_STAFF_CHECKBOX = (By.CSS_SELECTOR, "#id_is_staff")
    LOCATOR_SUPERUSER_CHECKBOX = (By.CSS_SELECTOR, "#id_is_superuser")
    LOCATOR_SAVE_BTN = (By.CSS_SELECTOR, ".default")
    LOCATOR_SUCCESS = (By.CSS_SELECTOR, ".success")
