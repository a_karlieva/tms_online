from selenium.webdriver.common.by import By


class AdminPageLocators:
    """
    Class of AdminPage Locators
    """

    LOCATOR_ADMIN_HEADER = (By.CSS_SELECTOR, "a[href='/admin/']")
    LOCATOR_ADD_USER_LINK = (By.CSS_SELECTOR,
                             "a[href='/admin/auth/user/add/']")
    LOCATOR_POSTS = (By.CSS_SELECTOR, "a[href='/admin/app/post/']")
