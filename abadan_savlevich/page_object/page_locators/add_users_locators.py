from selenium.webdriver.common.by import By


class UsersPageLocators:
    """
    Class of UsersPage Locators
    """

    LOCATOR_USERNAME = (By.CSS_SELECTOR, "#id_username")
    LOCATOR_PASSWORD = (By.CSS_SELECTOR, "#id_password1")
    LOCATOR_CONFIRM_PASSWORD = (By.CSS_SELECTOR, "#id_password2")
    LOCATOR_BTN = (By.CSS_SELECTOR, ".default")
    LOCATOR_SUCCESS = (By.CSS_SELECTOR, ".success")
