from selenium.webdriver.common.by import By


class ChangePostPageLocators:
    """
    Class of ChangePost Locators
    """

    LOCATOR_DELETE_BTN = (By.CSS_SELECTOR, ".deletelink")
    LOCATOR_YES_BTN = (By.CSS_SELECTOR, "input[type = 'submit']")
    LOCATOR_SUCCESS = (By.CSS_SELECTOR, ".success")
    LOCATOR_IMG_ID = (By.CSS_SELECTOR, "#id_photo")
