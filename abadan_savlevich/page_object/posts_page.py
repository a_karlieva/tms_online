from selenium.webdriver.support.ui import Select
from .base_page import BasePage
from .page_locators.posts_page_locators import PostsPageLocators
from .change_post_page import ChangePostPage


class PostsPage(BasePage):
    """
    Class of PostsPage, inherited from the BasePage.
    """

    def get_title(self):
        """
        Function of getting page title
        :return: page title
        """
        return self.driver.title

    def click_checkbox(self):
        """
        Function of selecting checkboxes of posts on the page
        :return: True or False
        """
        link = self.find_element(PostsPageLocators.LOCATOR_CHECKBOX)
        link.click()
        checkbox = self.find_element(
            PostsPageLocators.LOCATOR_CHECKED_CHECKBOX)
        return checkbox

    def delete_post(self):
        """
        Function of deleting posts on the page
        :return: True or False
        """
        select = Select(self.find_element(PostsPageLocators.LOCATOR_DROPDOWN))
        select.select_by_visible_text("Delete selected posts")
        btn = self.find_element(PostsPageLocators.LOCATOR_GO_BTN)
        btn.click()
        yes_btn = self.find_element(PostsPageLocators.LOCATOR_YES)
        yes_btn.click()
        success_message = self.find_element(PostsPageLocators.LOCATOR_SUCCESS)
        return success_message

    def open_post(self):
        """
        Function of opening posts
        :return: opens ChangePostPage
        """
        first_row = self.find_element(PostsPageLocators.LOCATOR_FIRST_ROW)
        first_row.click()
        return ChangePostPage(self.driver, self.driver.current_url)
