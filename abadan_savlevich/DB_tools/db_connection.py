import psycopg2


class DBHelper:
    def __init__(self):
        """
        Attributes of instance class
        """
        self.dbname = "postgres"
        self.user = "postgres"
        self.password = "postgres"
        self.host = "host.docker.internal"

    def __connect__(self):
        """
        Function to connect to DB
        """
        self.connection = psycopg2.connect(dbname=self.dbname,
                                           user=self.user,
                                           password=self.password,
                                           host=self.host)
        self.cur = self.connection.cursor()

    def __disconnect__(self):
        """
        Function to disconnect from the DB
        """
        self.connection.close()

    def execute(self, sql):
        """
        Function to execute sql request
        :param sql: sql request
        """
        self.__connect__()
        self.cur.execute(sql)
        self.__disconnect__()

    def delete(self, sql):
        """
        Function to execute sql request
        :param sql: sql request
        """
        self.__connect__()
        self.cur.execute(sql)
        self.connection.commit()
        self.__disconnect__()

    def fetch(self, sql):
        """
        Function that fetches all the rows of a query result
        :param sql: sql request
        :return: query result
        """
        self.__connect__()
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def convert_to_list(self, sql):
        """
        Function to convert the response to the list
        :param sql:
        :return:
        """
        result_list = self.fetch(sql)
        new = []
        for i in result_list:
            new.append("".join(list(i)))
        self.__disconnect__()
        return new
