import allure
from ..page_object.main_page import MainPage
from ..DB_tools.sql_requests import db_list, delete_sql


@allure.feature("Posts")
@allure.story("Deleting posts from admin")
def test_delete_post(driver, arrange_user):
    """
    Test function to verify successful deletion of a post
    :param driver: chrome driver
    """
    # open main page
    main_page = MainPage(driver)
    main_page.open()
    # login with admin user
    login_page = main_page.open_login_page()
    admin_page = login_page.login(arrange_user[0], arrange_user[1])
    with allure.step("Check main page title"):
        assert admin_page.get_title() == "Site administration" \
                                         " | Django site admin", \
            "Incorrect admin page title!"
    # go to posts page
    posts_page = admin_page.go_to_posts_page()
    with allure.step("Check posts page title"):
        assert posts_page.get_title() == "Select post to change" \
                                         " | Django site admin", \
            "Incorrect posts page title!"
    # go to change posts page
    change_post_page = posts_page.open_post()
    # get img value
    admin_img = change_post_page.get_img_src()
    img_src = admin_img.get_attribute("value")
    # delete the post
    delete_msg = change_post_page.delete_post()
    with allure.step("Check deleting success message"):
        assert bool(delete_msg) is True, "Deleting message wasn't presented"
    # open main page
    main_page.open()
    # get list of img values
    main_img = main_page.get_img_value_list()
    # check deleted post is not on the main pag
    with allure.step("Check deleted post is not on the main page"):
        assert img_src not in main_img, "Post is still on" \
                                        " the main page"


@allure.feature("Users")
@allure.story("Adding new user to admin")
def test_add_user(driver, arrange_user):
    """
    Test function to verify successful creation of a new user
    :param driver: chrome driver
    """
    # open main page
    main_page = MainPage(driver)
    main_page.open()
    # login with admin user
    login_page = main_page.open_login_page()
    admin_page = login_page.login(arrange_user[0], arrange_user[1])
    with allure.step("Check main page title"):
        assert admin_page.get_title() == "Site administration" \
                                         " | Django site admin", \
            "Incorrect admin page title!"
    add_user_page = admin_page.go_to_add_users_page()
    # create new user
    new_user = add_user_page.add_user(arrange_user[2], arrange_user[3])
    with allure.step("Check page title after adding new user"):
        assert new_user.get_title() == f"{arrange_user[2]} | Change user |" \
                                       f" Django site admin", \
            "User wasn't created"
    # check user in DB
    with allure.step("Check new user appeared in DataBase"):
        assert arrange_user[2] in db_list(), "User not found in Data Base"
    # add permissions to the user
    permission = new_user.click_checkbox()
    with allure.step("Check all permissions were assigned to the new user"):
        assert bool(permission) is True, "Impossible to add permissions"
    # logout
    logout_page = admin_page.logout()
    with allure.step("Check logout page title"):
        assert logout_page.get_title() == "Logged out | Django site admin",\
            "User couldn't log out"
    # login with new user
    main_page.open()
    login_new_user = main_page.open_login_page()
    login_new_user = login_new_user.login(arrange_user[2], arrange_user[3])
    with allure.step("Check main page title after login with new user"):
        assert login_new_user.get_title() == "Site administration |" \
                                             " Django site admin", \
            "Incorrect admin page title!"
    # delete the user from DB
    delete_sql(arrange_user[2])
    with allure.step("Check the new user was deleted"):
        assert arrange_user[2] not in db_list(), "User found in Data Base"
