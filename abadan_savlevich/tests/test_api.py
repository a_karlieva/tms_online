import time
from ..RestApi_tools.rest_api import RESTHelper
import allure


@allure.feature("API tests")
@allure.story("Adding new pet and updating his name")
def test_add_pet(get_files):
    """
    Test function to verify successful adding and updating pet
    """
    # create new pet with data from parsed JSON file
    new_pet = RESTHelper().post(get_files[0])
    with allure.step("Check the success status of creation"):
        assert new_pet == 200

    # get id from the file
    id_json = get_files[0]["id"]

    # get pet info with id from the file
    pet_info = RESTHelper().get(id_json)

    # verify the right id from bd and from the file
    with allure.step("Get created pet by id"):
        assert pet_info["id"] == id_json, "Pet by 'id' not found"

    # update name for the same pet
    update_new_pet = RESTHelper().update(get_files[1])
    time.sleep(25)
    with allure.step("Check the success status of pet updating"):
        assert update_new_pet == 200

    # get name from the file
    name_json = get_files[1]["name"]

    # get pet info by id
    pet_info_new = RESTHelper().get(id_json)

    # verify the right name from bd and from the file
    with allure.step("Get updated pet name by id"):
        assert pet_info_new["name"] == name_json, \
            f"Pet with name {name_json} wasn't found'"
