import pytest
from selenium import webdriver
from pathlib import Path
from ..RestApi_tools.get_json import get_json
from random_username.generate import generate_username


@pytest.fixture()
def driver():
    """
    :return: Chrome browser
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    driver = webdriver.Chrome(options=chrome_options)
    driver.maximize_window()
    driver.implicitly_wait(5)
    yield driver
    driver.quit()


@pytest.fixture(scope="function")
def arrange_user():
    """
    Fixture of admin users
    :return: username and password for admin and new user
    """
    user = 'admin'
    password = 'password'
    new_username = ''.join(generate_username(1))
    new_password = 'passwordsavlevich'
    return [user, password, new_username, new_password]


@pytest.fixture(scope="function")
def get_files():
    """
    Fixture of getting json files and parse them
    :return: parsed_new_file and parsed_update_file
    """
    new_file = Path("./abadan_savlevich/RestApi_tools/new_pet.json")
    update_file = Path("./abadan_savlevich/RestApi_tools/update.json")
    parsed_new_file = get_json(new_file)
    parsed_update_file = get_json(update_file)
    return parsed_new_file, parsed_update_file
