import json


def get_json(file_name) -> dict:
    """
    Function of getting JSON file.
    :return: dictionary.
    """
    file_obj = open(file_name, "r")
    content_json = json.load(file_obj)
    file_obj.close()
    return content_json
