import json
from requests import post, get, put


class RESTHelper:

    URL = "https://petstore.swagger.io/v2/"

    def post(self, pet_data):
        """
        Function of creating pet
        :param pet_data: pet data
        :return: status code
        """
        response = post(url=f"{self.URL}pet", json=pet_data)
        return response.status_code

    def get(self, param) -> dict:
        """
        Function of getting pet info
        :param param: id
        :return: dictionary
        """
        response = get(url=f"{self.URL}pet/{param}")
        info = json.loads(response.content)
        return info

    def update(self, pet_data):
        """
        Function of updating pet info
        :param pet_data: pet data
        :return: status code
        """
        response = put(url=f"{self.URL}pet", json=pet_data)
        return response.status_code
